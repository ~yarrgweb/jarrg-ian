#!/bin/bash
#
# We don't supply a Makefile.  Instead, we just supply this shell script.

set -e

case "$#.$1" in
0.)	;;
*.-*)	echo >&2 'no options allowed'; exit 1;;
*)	echo >&2 'no arguments allowed'; exit 1;;
esac

: ${JAVA_HOME:=/usr/lib/jvm/java-8-openjdk-amd64}
export JAVA_HOME

revision=`git describe --always || echo '(unknown revision)'`
if [ x"`git diff 2>/dev/null || echo x`" != x ]; then
	revision="$revision-local"
fi
revision=${revision#jarrg-}

cat >src/net/chiark/yarrg/Version.java <<END
  package net.chiark.yarrg;
  public class Version {
      public final static String version = "$revision";
  }
END
rm -f src/build/net/chiark/yarrg/Version.class
rm -f src/build/net/chiark/yarrg/ControlPanel.class
rm -f src/build/net/chiark/yarrg/MarketUploader.class
rm -f *.jar

cd src
ant -f Jarrg.xml
cd ..

echo "
Building tarballs
"

tarball () {
	output=$1; shift
	>"$output"; # avoids rsync seeing that . changed
	GZIP='-9v --rsyncable' tar --exclude=\*{~,.tar.gz,.exe} --exclude='#*#' \
	  --transform='s/^./jarrg/' --exclude={src/build,tmp} "$@" \
	  -zcf "$output" .
}

tarball jarrg-source.tar.gz

tarball jarrg-linux.tar.gz \
  --exclude={.git*,src,accessibility.properties,build-sh} \

files='jarrg-source.tar.gz jarrg-linux.tar.gz jarrg-setup.exe'

echo "
Generated OK:
"
ls -al $files

pubbase="${JARRG_PUBLISH_BASE-ijackson@login.chiark.greenend.org.uk}"
pubdir="${JARRG_PUBLISH_DIR-/home/yarrgweb/public-html/download/jarrg}"

echo "
Revision: $revision

Publish as test with:
  RSYNC_RSH=ssh rsync -vP $files $pubbase:$pubdir/test/

To actually publish a release, on $pubbase:
  mkdir $revision
  ln test/* $revision/.
  ln -f test/* .
"
