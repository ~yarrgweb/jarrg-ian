package net.chiark.yarrg;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.prefs.*;

/**
*	ControlPanel is a simple management utility that sets
*	a preference for which server(s) to upload to.
*/
public class ControlPanel extends JFrame {
	public static void main(String[] args) {
		new ControlPanel();
	}
	
	public ControlPanel() {
		super("Jarrg Settings");
		final Preferences prefs = Preferences.userNodeForPackage(getClass());
		final JCheckBox toPCTB = new JCheckBox("Upload to PCTB?", prefs.getBoolean("uploadToPCTB", false));
		final JCheckBox toYarrg = new JCheckBox("Upload to Yarrg?", prefs.getBoolean("uploadToYarrg", true));
		final JCheckBox showArbitrage = new JCheckBox("Show arbitrage?", prefs.getBoolean("showArbitrage", true));

		final JRadioButton live = new JRadioButton("Use live servers");
		final JRadioButton testing = new JRadioButton("Use testing servers");
		
		live.setSelected(prefs.getBoolean("useLiveServers", true));
		testing.setSelected(!prefs.getBoolean("useLiveServers", true));

		ButtonGroup liveortest = new ButtonGroup();
		liveortest.add(live);
		liveortest.add(testing);

		String version_label = " version: " +
		    net.chiark.yarrg.Version.version;
		JLabel version = new JLabel(version_label);

		final JCheckBox enableDebug = new JCheckBox("Write debug files?", prefs.getBoolean("writeDebugFiles", false));

		setLayout(new GridLayout(8,1));
		add(toYarrg);
		add(toPCTB);
		add(showArbitrage);
		add(live);
		add(testing);
		add(version);
		add(enableDebug);

		final int exitstatus = Integer.parseInt(System.getProperty("net.chiark.yarrg.controlpanel.exitstatus", "0"));

		JButton but = new JButton("Save options");
		add(but);
		but.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				prefs.putBoolean("uploadToPCTB", toPCTB.isSelected());
				prefs.putBoolean("uploadToYarrg", toYarrg.isSelected());
				prefs.putBoolean("showArbitrage", showArbitrage.isSelected());
				prefs.putBoolean("useLiveServers", live.isSelected());
				prefs.putBoolean("writeDebugFiles", enableDebug.isSelected());
				System.exit(exitstatus);
			}
		});
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setSize(getWidth() + 10, getHeight() + 10);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getRootPane().setDefaultButton(but);
	}
}
